

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'comic.dart';

Future<Comic> fetchComic() async{
  final respons =
  await http.get(Uri.https('xkcd.com', 'info.0.json'));
  if (respons.statusCode ==200){
    return Comic.fromJson(jsonDecode(respons.body));
  }else{
    throw Exception('Failed to load last comic');
  }
}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<Comic> futureComic;

  @override
  void initState() {
    super.initState();
    futureComic = fetchComic();
  }


  @override
  Widget build(BuildContext context) {
    var title = 'XKCD Viewer';
    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body:Center(
            child: FutureBuilder<Comic>(
                future: futureComic,
                builder: (context,snapshot){
                  if(snapshot.hasData){
                    return Image.network(snapshot.data!.img);
                  }else if (snapshot.hasError){
                    return Text('error loding data');
                  }
                  return CircularProgressIndicator();
                }
            ),
        ),
      ),
    );
  }


}


