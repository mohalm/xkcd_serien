class Comic {
  final int num;
  final String img;
  final String title;

  Comic({required this.num,
         required this.img,
         required this.title});

  factory Comic.fromJson(Map<String,dynamic>json){
    return Comic(num: json['num'], img: json['img'], title: json['title'],);
  }

}